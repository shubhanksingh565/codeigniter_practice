<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Test_controller extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      $this->load->model('Test_model', 'model1');
   }

   public function loadview()
   {
      $postdata = ['id' => 1];
      $data['data1'] = $this->model1->get_all_items($postdata['id']);
      $this->load->view('test_view', $data);
   }
}

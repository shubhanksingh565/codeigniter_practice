<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Test_model extends CI_model
{
   public function get_all_items($id)
   {
      $this->db->select('*');
      $this->db->where('id', $id);
      $result = $this->db->get('test_table')->result_array();
      // echo $this->db->last_query();exit;
      return $result;
   }
}

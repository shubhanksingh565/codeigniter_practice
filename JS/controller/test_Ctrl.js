angular
    .module('app')
    .controller('test_Ctrl', test_Ctrl);

function test_Ctrl($scope, $http, $state, $rootScope, $timeout) {
    $scope.touchMe = function () {
        console.log('testing controller');
    };
}
angular
    .module('app')
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

        const basePATH = 'http://localhost/CodeIgniterPractice/';

        $urlRouterProvider.otherwise('test');

        $stateProvider
            .state('test', {
                url: '/test',
                templateUrl: basePATH + 'test',
                controller: 'test_Ctrl',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['controller/test_Ctrl.js']
                        });
                    }]
                }
            });
    }]);